
NDefines.NGraphics.TRAILS_ALPHA_FADE = 1.0 -- Controls of quick we alpha fade-out missile trails
NDefines.NGraphics.STRIKE_CRAFT_TRAIL_FADE_RATE = 2.5 
NDefines.NCombat.DAMAGE_REDUCTION = 30    -- Improved Armor efficiency standard 60  formula = armor/(armor + value)
NDefines.NEconomy.MILITARY_STATION_MAINTENANCE_MUL	= 0.00	-- Monthly military station maintenance multiplier
NDefines.NInterface.TRADE_VIEW_SMALL_CREDIT_CHANGE			= 50	-- Decides how much value is changed while holding control when increasing/decreasing credit in trade
NDefines.NInterface.TRADE_VIEW_LARGE_CREDIT_CHANGE			= 1000	-- Decides how much value is changed while holding shift when increasing/decreasing credit in trade
NDefines.NGameplay.CORE_SECTOR_PLANET_CAP					= 7	-- Too many planets in core sector will apply modifier inefficient_planet_management.	
-- Better Sector Mod changes
-- NDefines.NGameplay.SECTOR_SUPPORT_RESOURCE_AMOUNT = 100;	-- How much resources each click will give the sector
NDefines.NGameplay.SECTOR_REVOKE_PLANET_COST = 25;			-- Influence cost for revoking a planet from a sector
NDefines.NGameplay.DELETE_SECTOR_COST = 50;					-- Influence cost for deleting a sector
-- NDefines.NAI.SECTOR_STATION_BUDGET_FRACTION = 0.20;			-- Fraction of budget going to stations
-- NDefines.NAI.SECTOR_BUILDING_BUDGET_FRACTION = 0.45;		-- Fraction of budget going to buildings
-- NDefines.NAI.SECTOR_SPACEPORT_BUDGET_FRACTION = 0.30;		-- Fraction of budget going to spaceports
-- NDefines.NAI.SECTOR_ARMY_BUDGET_FRACTION = 0.05;			-- Fraction of budget going to armies
-- NDefines.NAI.SECTOR_BUILDING_MAINT_BUDGET_FRACTION = 0.55;	-- Fraction of maintenance budget going to buildings
-- NDefines.NAI.SECTOR_STATION_MAINT_BUDGET_FRACTION = 0.40;	-- Fraction of maintenance budget going to spaceports and stations
-- NDefines.NAI.SECTOR_ARMY_MAINT_BUDGET_FRACTION = 0.05;		-- Fraction of maintenance budget going to armies
-- NDefines.NAI.POP_FOOD_MIN = 2;								-- AI should always prioritize to get more food when planet food tot is below this value
